# NOTES
## Setup

1. Navigate to data folder in the code directory, run yarn, then run yarn start
2. Navigate to server folder in the code directory, run yarn, then run yarn start
3. Navigate to the code/client/clearpoint directory, run yarn, then run yarn start
4. Wait for the site to open in your browser



## Assumptions

- I assumed the width of the screen would fill up with as many employee cards as possible instead of expanding employee cards to keep it at 3 always.
- I assumed this is SPA with only one route so I did not use react-router or something similar.
- I only stored app state data in the redux store and then stored business data in apollo cache.
- I left a bunch of TODO comments to let you know I thought of certain things because I tried to keep this to a MVP.
- As the functional requirements did not mention filtering employees I left out the filters from the UI but if this was a requirement please let me know and I will implement them.
- The modal in particular needs more battle testing with different media queries as I implemented a very basic responsive component.
- The server projects were kept simple as I assumed this is a react assessment, if I assumed wrong I would be happy to improve on them.